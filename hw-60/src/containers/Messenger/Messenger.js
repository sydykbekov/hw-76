import React, {Component} from 'react';
import Message from '../../components/Message/Message';
import './Messenger.css';

const URL = 'http://146.185.154.90:8000/messages';

class Messenger extends Component {
    state = {
        messages: [],
        lastDate: '',
        myMessage: {
            author: '',
            message: ''
        }
    };

    scrollToBottom = () => {
        const posts = document.getElementById('posts');
        posts.scrollTop = posts.scrollHeight - posts.clientHeight;
    };

    addMessageAuthor = event => {
        const myMessage = this.state.myMessage;
        myMessage.author = event.target.value;
        this.setState({myMessage})
    };

    addMessageText = event => {
        const myMessage = this.state.myMessage;
        myMessage.message = event.target.value;
        this.setState({myMessage})
    };

    componentWillUnmount() {
        this.clearInt();
    }

    setInt = () => {
        this.interval = setInterval(() => {
            fetch(URL + '?datetime=' + this.state.lastDate).then(response => {
                if (response.ok) {
                    return response.json();
                }

                throw new Error('Something went wrong!');
            }).then(messages => {
                if (messages.length !== 0) {
                    const currentMessages = [...this.state.messages];
                    let newMessages = currentMessages.concat(messages);
                    this.setState({messages: newMessages, lastDate: newMessages[newMessages.length - 1].datetime});
                    this.scrollToBottom();
                }
            });
        }, 2000);
    };

    clearInt = () => {
        clearInterval(this.interval);
    };

    getMessages = () => {
        fetch(URL).then(response => {
            if (response.ok) {
                return response.json();
            }

            throw new Error('Something went wrong!');
        }).then(messages => {
            this.setState({messages: messages, lastDate: messages[messages.length - 1].datetime});
            this.scrollToBottom();
        });
    };


    componentDidMount() {
        this.getMessages();
        this.setInt();

    }

    sendMessage = () => {
        this.clearInt();

        let formData = new URLSearchParams();
        formData.append('message', this.state.myMessage.message);
        formData.append('author', this.state.myMessage.author);

        fetch(URL, {
            method: 'POST',
            body: formData
        }).then(function (response) {
            if (response.ok) {
                return response.json();
            }

            throw new Error('Something went wrong!');
        }).then(response => {
            this.setState({
                myMessage: {
                    message: '',
                    author: ''
                }
            })
        });

        this.getMessages();
        this.setInt();

    };

    render() {
        return (
            <div className="chat">
                <div className="chat-header">
                    <h4>Messenger</h4>
                </div>
                <div className="chat-body" id="posts">
                    {this.state.messages.map(message => {
                        return <Message
                            key={message._id}
                            author={message.author}
                            message={message.message}
                            time={message.datetime}
                        />
                    })}
                </div>
                <div className="chat-footer">
                    <form action="#">
                        <input onChange={this.addMessageAuthor} value={this.state.myMessage.author} type="text"
                               placeholder="Author"/>
                        <textarea placeholder="Message..." onChange={this.addMessageText}
                                  value={this.state.myMessage.message}/>
                        <button onClick={this.sendMessage}>Send</button>
                    </form>
                </div>
            </div>
        )
    }
}

export default Messenger;