import React, { Component } from 'react';
import Messenger from './containers/Messenger/Messenger';

class App extends Component {
  render() {
    return (
      <Messenger/>
    );
  }
}

export default App;
