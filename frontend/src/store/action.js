import axios from 'axios';

export const START_REQUEST = 'START_REQUEST';
export const SUCCESS_REQUEST = 'SUCCESS_REQUEST';
export const ERROR_REQUEST = 'ERROR_REQUEST';
export const LAST_DATE = 'LAST_DATE';

const startRequest = () => {
    return {type: START_REQUEST};
};

const successRequest = (messages) => {
    return {type: SUCCESS_REQUEST, messages};
};

const errorRequest = () => {
    return {type: ERROR_REQUEST};
};

const lastDate = (date) => {
    return {type: LAST_DATE, date};
};

const scrollToBottom = () => {
    const posts = document.getElementById('posts');
    posts.scrollTop = posts.scrollHeight - posts.clientHeight;
};

export const clearInt = () => {
    clearInterval(this.interval);
};

export const setInt = () => {
    return (dispatch, getState) => {
        this.interval = setInterval(() => {
            axios.get(`?datetime=${getState().lastDate}`).then(response => {
                if (response.data.length !== 0) {
                    const currentMessages = getState().messages;
                    let newMessages = currentMessages.concat(response.data);
                    dispatch(successRequest(newMessages));
                    dispatch(lastDate(newMessages[newMessages.length - 1].datetime));
                    scrollToBottom();
                }
            });
        }, 2000);
    }
};

export const getMessages = () => {
    return dispatch => {
        dispatch(startRequest());
        axios.get('messages').then(response => {
            dispatch(successRequest(response.data));
            scrollToBottom();
        }, error => {
            dispatch(errorRequest());
        });
    };
};

export const sendMessage = (state) => {
    this.clearInt();
    return () => {
        axios.post('messages', state).then(() => {
            getMessages();
            setInt();
        });
    };
};