import React, {Component} from 'react';
import Messanger from './containers/Messanger/Messenger';
import './App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Messanger />
            </div>
        );
    }
}

export default App;
